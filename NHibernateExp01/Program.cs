﻿using System;
using NHibernateExp01.Entities;

namespace NHibernateExp01
{
    class Program
    {

        static void Main(string[] args)
        {
            var sessionFactory = SessionDb.CreateSessionFactory();
            var person1 = new Person() { Name = "Pessoa 1" };
            var person2 = new Person() { Name = "Pessoa 2" };
            var person3 = new Person() { Name = "Pessoa 3" };

            person1.PersonAutorizes.Add(new PersonAutorize() { Id = person1, Autorize = person2 });

            var group1 = new PersonAutorizeGroup() { Id = person1, Autorize = person2, GroupId = 1 };
            var group2 = new PersonAutorizeGroup() { Id = person1, Autorize = person2, GroupId = 2 };
            var group3 = new PersonAutorizeGroup() { Id = person1, Autorize = person2, GroupId = 3 };
            person1.PersonAutorizes[0].Groups.Add(group1);
            person1.PersonAutorizes[0].Groups.Add(group2);
            person1.PersonAutorizes[0].Groups.Add(group3);

            using (var session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {

                    session.SaveOrUpdate(person2);
                    session.SaveOrUpdate(person3);
                    session.SaveOrUpdate(person1);

                    transaction.Commit();
                }
            }

            using (var session = sessionFactory.OpenSession())
            {
                using (session.BeginTransaction())
                {
                    var persons = session.CreateCriteria(typeof(Person)).List<Person>();

                    foreach (var p in persons)
                    {
                        using (var transaction = session.BeginTransaction())
                        {
                            session.SaveOrUpdate(p);
                            transaction.Commit();
                        }
                    }

                }
            }
            Console.ReadKey();
        }
    }
}

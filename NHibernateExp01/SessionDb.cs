﻿using System;
using System.Collections.Generic;
using System.IO;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace NHibernateExp01
{
    public static class SessionDb
    {
        private const string DbFile = "firstProgram.db";
        private const string SqlServerConnectionString = "Data Source=localhost;Initial Catalog=NHIBERNATE-TESTES;User Id=sa;Pooling=true;Min Pool Size=1;Max Pool Size=60;Password=mci523dhallw9";
        public static ISessionFactory CreateSessionFactory()
        {
            // Sqlite
            //return Fluently.Configure()
            //    .Database(SQLiteConfiguration.Standard
            //        .UsingFile(DbFile))
            //    .Mappings(m =>
            //        m.FluentMappings.AddFromAssemblyOf<Program>())
            //    .ExposeConfiguration(BuildSchema)
            //    .BuildSessionFactory();

            // SqlServer
            return Fluently.Configure()
                .Database(MsSqlConfiguration.MsSql2012.ConnectionString(SqlServerConnectionString))
                .Mappings(m =>
                    m.FluentMappings.AddFromAssemblyOf<Program>())
                .ExposeConfiguration(x => x.SetInterceptor(new SqlStatementInterceptor()))
                .BuildSessionFactory();

        }

        private static void BuildSchema(Configuration config)
        {
            // delete the existing db on each run
            if (File.Exists(DbFile))
                File.Delete(DbFile);

            // this NHibernate tool takes a configuration (with mapping info in)
            // and exports a database schema from it
            new SchemaExport(config)
                .Create(false, true);
        }

    }
}

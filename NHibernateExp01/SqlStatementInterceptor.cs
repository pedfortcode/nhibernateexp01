﻿using NHibernate;
using System.Diagnostics;

namespace NHibernateExp01
{
    public class SqlStatementInterceptor : EmptyInterceptor
    {
        public override NHibernate.SqlCommand.SqlString OnPrepareStatement(NHibernate.SqlCommand.SqlString sql)
        {
            Trace.WriteLine(sql.ToString());

            foreach (var p in sql.GetParameters())
            {
                Trace.WriteLine("Parameters: " + p.ToString()); 
            }

            return sql;
        }
    }
}

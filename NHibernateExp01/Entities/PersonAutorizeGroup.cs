﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateExp01.Entities
{
    public class PersonAutorizeGroup
    {
        public PersonAutorizeGroup()
        {
        }
        public virtual Person Id { get; set; }

        public virtual Person Autorize { get; set; }

        public virtual int GroupId { get; set; }


        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            var t = obj as PersonAutorizeGroup;

            if (t == null) return false;
            if (Id == t.Id && Autorize == t.Autorize && GroupId == t.GroupId)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^ Autorize.GetHashCode() ^ GroupId.GetHashCode();
        }


    }
}

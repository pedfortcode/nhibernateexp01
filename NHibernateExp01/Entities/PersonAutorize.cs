﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateExp01.Entities
{
    public class PersonAutorize
    {
        public PersonAutorize()
        {
            Groups = new List<PersonAutorizeGroup>();
        }
        public virtual Person Id { get; set; }

        public virtual Person Autorize { get; set; }

        public virtual IList<PersonAutorizeGroup> Groups { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;

            var t = obj as PersonAutorize;

            if (t == null) return false;
            if (Id == t.Id && Autorize == t.Autorize)
                return true;

            return false;
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode() ^ Autorize.GetHashCode();
        }

    }
}

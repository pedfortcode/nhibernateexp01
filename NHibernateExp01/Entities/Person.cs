﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NHibernateExp01.Entities
{
    public class Person
    {
        public Person()
        {
            PersonAutorizes = new List<PersonAutorize>();
        }
        public virtual int Id { get; protected set; }
        public virtual string Name { get; set; }
        public virtual IList<PersonAutorize> PersonAutorizes { get; set; }

        public override bool Equals(object obj)
        {
            Person p = obj as Person;
            if (p != null && p.Id == this.Id)
            {
                return true;
            }
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

    }
}

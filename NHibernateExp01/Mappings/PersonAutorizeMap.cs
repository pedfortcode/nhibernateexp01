﻿using FluentNHibernate.Mapping;
using NHibernateExp01.Entities;

namespace NHibernateExp01.Mappings
{
    public class PersonAutorizeMap : ClassMap<PersonAutorize>
    {
        public PersonAutorizeMap()
        {
            Table("PESSOA_AUTORIZADOR");

            CompositeId()
                .KeyReference(x => x.Id, "CD_PESSOA")
                .KeyReference(x => x.Autorize, "CD_PESSOA_AUTORIZADOR");

            HasMany((x => x.Groups))
                .Table("PESSOA_AUTORIZADOR_GRUPO").Not.LazyLoad()
                .KeyColumns.Add("CD_PESSOA", "CD_PESSOA_AUTORIZADOR").Not.KeyNullable().Inverse()
                //.Cascade.AllDeleteOrphan();
                .Cascade.All();

        }
    }
}

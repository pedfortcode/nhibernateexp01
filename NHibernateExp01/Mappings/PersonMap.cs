﻿using FluentNHibernate.Mapping;
using NHibernateExp01.Entities;

namespace NHibernateExp01.Mappings
{
    public class PersonMap : ClassMap<Person>
    {
        public PersonMap()
        {
            Table("PESSOA");

            Id(x => x.Id, "CD_PESSOA");

            Map(x => x.Name, "NM_PESSOA");

            HasMany((x => x.PersonAutorizes))
                .Table("PESSOA_AUTORIZADOR")
                .KeyColumn("CD_PESSOA").Not.KeyNullable()
                .Cascade.AllDeleteOrphan().Inverse();
                //.Cascade.All().Inverse();
        }
    }
}

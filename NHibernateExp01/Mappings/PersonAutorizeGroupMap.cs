﻿using FluentNHibernate.Mapping;
using NHibernateExp01.Entities;

namespace NHibernateExp01.Mappings
{
    public class PersonAutorizeGroupMap : ClassMap<PersonAutorizeGroup>
    {
        public PersonAutorizeGroupMap()
        {
            Table("PESSOA_AUTORIZADOR_GRUPO");

            CompositeId()
                .KeyReference(x => x.Id, "CD_PESSOA")
                .KeyReference(x => x.Autorize, "CD_PESSOA_AUTORIZADOR")
                .KeyProperty(x => x.GroupId, "CD_GRUPO");

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NHibernateExp01;
using NHibernateExp01.Entities;

namespace UnitTestNHibernateExp01
{
    [TestClass]
    public class PersonTest
    {
        [TestMethod]
        public void InsertPersonWithPersonAutorizesAndGroups()
        {
            var sessionFactory = SessionDb.CreateSessionFactory();
            var person1 = new Person() { Name = "Pessoa 1" };
            var person2 = new Person() { Name = "Pessoa 2" };
            var person3 = new Person() { Name = "Pessoa 3" };

            person1.PersonAutorizes.Add(new PersonAutorize() { Id = person1, Autorize = person2 });

            var group1 = new PersonAutorizeGroup() { Id = person1, Autorize = person2, GroupId = 1 };
            var group2 = new PersonAutorizeGroup() { Id = person1, Autorize = person2, GroupId = 2 };
            var group3 = new PersonAutorizeGroup() { Id = person1, Autorize = person2, GroupId = 3 };
            person1.PersonAutorizes[0].Groups.Add(group1);
            person1.PersonAutorizes[0].Groups.Add(group2);
            person1.PersonAutorizes[0].Groups.Add(group3);

            using (var session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {

                    session.SaveOrUpdate(person2);
                    session.SaveOrUpdate(person3);
                    session.SaveOrUpdate(person1);

                    transaction.Commit();
                }
            }
        }

        [TestMethod]
        public void SelectPersons()
        {
            var sessionFactory = SessionDb.CreateSessionFactory();

            using (var session = sessionFactory.OpenSession())
            {
                using (session.BeginTransaction())
                {
                    var persons = session.CreateCriteria(typeof(Person)).List<Person>();

                }
            }
        }

        [TestMethod]

        public void DeleteAll()
        {
            var sessionFactory = SessionDb.CreateSessionFactory();

            using (var session = sessionFactory.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    var persons = session.CreateCriteria(typeof(Person)).List<Person>();

                    foreach (var person in persons)
                    {
                        foreach (var pa in person.PersonAutorizes)
                        {
                            session.Delete(pa);
                        }
                        //session.Delete(person);
                    }
                    transaction.Commit();
                }
            }
        }

        [TestMethod]
        public void UpdateGroup()
        {
            var sessionFactory = SessionDb.CreateSessionFactory();

            using (var session = sessionFactory.OpenSession())
            {
                using (session.BeginTransaction())
                {
                    var persons = session.CreateCriteria(typeof(Person)).List<Person>();

                    foreach (var p in persons.Where(x => x.Id == 225).ToList())
                    {
                        if(p.PersonAutorizes != null && p.PersonAutorizes.Count > 0)
                        {
                            List<PersonAutorizeGroup> personAutorizeGroupsNew = new List<PersonAutorizeGroup>();

                            PersonAutorizeGroup gremoved = null;

                            if (p.PersonAutorizes[0].Groups != null && p.PersonAutorizes[0].Groups.Count > 0)
                            {
                                PersonAutorizeGroup newG = new PersonAutorizeGroup()
                                {
                                    Id = p.PersonAutorizes[0].Id,
                                    Autorize = p.PersonAutorizes[0].Autorize,
                                    GroupId = 15
                                };

                                //p.PersonAutorizes[0].Groups.RemoveAt(0);
                                gremoved = p.PersonAutorizes[0].Groups[0];
                                p.PersonAutorizes[0].Groups.RemoveAt(0);
                                p.PersonAutorizes[0].Groups.Add(newG);
                            }

                            using (var transaction = session.BeginTransaction())
                            {
                                if (gremoved != null)
                                    //session.Delete(gremoved);

                                session.SaveOrUpdate(p);

                                transaction.Commit();

                            }
                        }
                    }
                }
            }
        }
    }
}
